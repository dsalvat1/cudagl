
# Announcement!

## Publishing of new cudagl image tags are on temporary hiatus

cudagl image updates have been put on hold until we can implement improved support in our internal CI/CD system. This means new cudagl image tags for new toolkit versions will not be published until we have implemented the required changes.

We will continue updating existing image tags to capture CVE updates.

Work on new pipeline automation is expected to begin late Q2-2022.

Until that date, it is possible to build these images using our [build.sh](https://gitlab.com/nvidia/container-images/cuda/-/blob/master/build.sh) helper script:

```
./build.sh -d --image-name <a_private_container_registry_url>/cudagl --cuda-version 11.6.2 --os ubuntu --os-version 20.04 --arch x86_64 --cudagl --push
```

# Ubuntu 20.04

## CUDA 11.4 Update 2 (11.4.2) + OpenGL (glvnd 1.2)

- `11.4.2-base`, `11.4.2-base-ubuntu20.04` [(*11.4.11.4.2/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.4.2/ubuntu2004/base/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/devel/Dockerfile)
- `11.4.2-runtime`, `11.4.2-runtime-ubuntu20.04` [(*11.4.2/runtime/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.4.2/ubuntu2004/runtime/Dockerfile) + [(*glvnd/runtime/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/runtime/Dockerfile)
- `11.4.2-devel`, `11.4.2-ubuntu20.04` [(*11.4.2/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.4.2/ubuntu2004/devel/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/devel/Dockerfile)

## CUDA 11.4 Update 1 (11.4.1) + OpenGL (glvnd 1.2)

- `11.4.1-base`, `11.4.1-base-ubuntu20.04` [(*11.4.1/base/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.4.1/ubuntu2004/base/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/devel/Dockerfile)
- `11.4.1-runtime`, `11.4.1-runtime-ubuntu20.04` [(*11.4.1/runtime/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.4.1/ubuntu2004/runtime/Dockerfile) + [(*glvnd/runtime/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/runtime/Dockerfile)
- `11.4.1-devel`, `11.4.1-devel-ubuntu20.04` [(*11.4.0/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.4.1/ubuntu2004/devel/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/devel/Dockerfile)

## CUDA 11.4 (11.4.0) + OpenGL (glvnd 1.2)

- `11.4.0-base`, `11.4.0-base-ubuntu20.04` [(*11.4.0/base/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.4.0/ubuntu2004/base/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/devel/Dockerfile)
- `11.4.0-runtime`, `11.4.0-runtime-ubuntu20.04` [(*11.4.0/runtime/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.4.0/ubuntu2004/runtime/Dockerfile) + [(*glvnd/runtime/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/runtime/Dockerfile)
- `11.4.0-devel`, `11.4.0-devel-ubuntu20.04` [(*11.4.0/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.4.0/ubuntu2004/devel/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/devel/Dockerfile)

## CUDA 11.3 Update 1 (11.3.1) + OpenGL (glvnd 1.2)

- `11.3.1-base`, `11.3.1-base-ubuntu20.04` [(*11.3.1/base/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.3.1/ubuntu2004/base/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/devel/Dockerfile)
- `11.3.1-runtime`, `11.3.1-runtime-ubuntu20.04` [(*11.3.1/runtime/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.3.1/ubuntu2004/runtime/Dockerfile) + [(*glvnd/runtime/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/runtime/Dockerfile)
- `11.3.1-devel`, `11.3.1-devel-ubuntu20.04` [(*11.3.1/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.3.1/ubuntu2004/devel/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/devel/Dockerfile)

## CUDA 11.3 (11.3.0) + OpenGL (glvnd 1.2)

- `11.3.0-base`, `11.3.0-base-ubuntu20.04` [(*11.3.0/base/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.3.0/ubuntu2004/base/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/devel/Dockerfile)
- `11.3.0-runtime`, `11.3.0-runtime-ubuntu20.04` [(*11.3.0/runtime/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.3.0/ubuntu2004/runtime/Dockerfile) + [(*glvnd/runtime/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/runtime/Dockerfile)
- `11.3.0-devel`, `11.3.0-devel-ubuntu20.04` [(*11.3.0/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.3.0/ubuntu2004/devel/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/devel/Dockerfile)

## CUDA 11.2 Update 2 (11.2.2) + OpenGL (glvnd 1.2)

- `11.2.2-base`, `11.2.2-base-ubuntu20.04` [(*11.2.2/base/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.2.2/ubuntu2004/base/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/devel/Dockerfile)
- `11.2.2-runtime`, `11.2.2-runtime-ubuntu20.04` [(*11.2.2/runtime/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.2.2/ubuntu2004/runtime/Dockerfile) + [(*glvnd/runtime/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/runtime/Dockerfile)
- `11.2.2-devel`, `11.2.2-devel-ubuntu20.04` [(*11.2.2/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.2.2/ubuntu2004/devel/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/devel/Dockerfile)

## CUDA 11.2 Update 1 (11.2.1) + OpenGL (glvnd 1.2)

- `11.2.1-base`, `11.2.1-base-ubuntu20.04` [(*11.2.1/base/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.2.1/ubuntu2004/base/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/devel/Dockerfile)
- `11.2.1-runtime`, `11.2.1-runtime-ubuntu20.04` [(*11.2.1/runtime/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.2.1/ubuntu2004/runtime/Dockerfile) + [(*glvnd/runtime/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/runtime/Dockerfile)
- `11.2.1-devel`, `11.2.1-devel-ubuntu20.04` [(*11.2.1/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.2.1/ubuntu2004/devel/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/devel/Dockerfile)

## CUDA 11.2 (11.2.0) + OpenGL (glvnd 1.2)

- `11.2.0-base`, `11.2.0-base-ubuntu20.04` [(*11.2.0/base/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.2.0/ubuntu2004/base/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/devel/Dockerfile)
- `11.2.0-runtime`, `11.2.0-runtime-ubuntu20.04` [(*11.2.0/runtime/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.2.0/ubuntu2004/runtime/Dockerfile) + [(*glvnd/runtime/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/runtime/Dockerfile)
- `11.2.0-devel`, `11.2.0-devel-ubuntu20.04` [(*11.2.0/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.2.0/ubuntu2004/devel/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/devel/Dockerfile)

## CUDA 11.1 Update 1 (11.1.1) + OpenGL (glvnd 1.2)

- `11.1.1-base`, `11.1.1-base-ubuntu20.04` [(*11.1.1/base/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.1.1/ubuntu2004/base/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/devel/Dockerfile)
- `11.1.1-runtime`, `11.1.1-runtime-ubuntu20.04` [(*11.1.1/runtime/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.1.1/ubuntu2004/runtime/Dockerfile) + [(*glvnd/runtime/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/runtime/Dockerfile)
- `11.1.1-devel`, `11.1.1-devel-ubuntu20.04` [(*11.1.1/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.1.1/ubuntu2004/devel/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/devel/Dockerfile)

## CUDA 11.0 Update 1 (11.0.3) + OpenGL (glvnd 1.2)

- `11.0.3-base`, `11.0.3-base-ubuntu20.04` [(*11.0.3/base/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.0.3/ubuntu2004/base/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/devel/Dockerfile)
- `11.0.3-runtime`, `11.0.3-runtime-ubuntu20.04` [(*11.0.3/runtime/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.0.3/ubuntu2004/runtime/Dockerfile) + [(*glvnd/runtime/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/runtime/Dockerfile)
- `11.0.3-devel`, `11.0.3-devel-ubuntu20.04` [(*11.0.3/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/cuda/blob/master/dist/11.0.3/ubuntu2004/devel/Dockerfile) + [(*glvnd/devel/Dockerfile*)](https://gitlab.com/nvidia/container-images/opengl/blob/ubuntu20.04/glvnd/devel/Dockerfile)
